import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String _textoResultado = "";
  TextEditingController _controllerAlcool = TextEditingController();
  TextEditingController _controllerGasolina = TextEditingController();

  void _calcular(){
    double precoAlcool = double.tryParse(_controllerAlcool.text);
    double precoGasolina = double.tryParse(_controllerGasolina.text);

    if(precoAlcool == null || precoGasolina == null){
      setState(() {
        _textoResultado = "Número Inválido digite numeros maiores que 0 e utilizando (.)";
      });

    }else{
      /*
      se o preco do alcool dividido pelo preco da gasolina
      for >= 0.7 e melhor abastecer com gasolina
      se nao e melhor utilizar alcool
       */

      if((precoAlcool / precoGasolina) >= 0.7){
        setState(() {
          _textoResultado = "Melhor abastecer com Gasolina";
        });
      }else{
        setState(() {
          _textoResultado = "Melhor abastecer com Alcool";
        });
      }
      _limpaCampos();
    }
  }

  void _limpaCampos(){
    _controllerAlcool.text = "";
    _controllerGasolina.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alcool ou Gasolina"),
      ),
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                  padding: EdgeInsets.all(22),
                child: Image.asset("lib/assets/gas.png", height: 110,),
              ),
              Padding(padding: EdgeInsets.only(bottom: 10),
              ),
              Text(
                "Saiba qual melhor opção para abastecer o seu carro",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25,
                  fontWeight: FontWeight.bold
                ),
              ),
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Preço Alcool ex : 1.59",
                ),
                style: TextStyle(fontSize: 18),
                controller: _controllerAlcool,
              ),
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Preço Gasolina ex : 3.59",
                ),
                style: TextStyle(fontSize: 18),
                controller: _controllerGasolina,
              ),
              Padding(
                  padding: EdgeInsets.only(top: 30),
                child: RaisedButton(
                  color: Colors.purple,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Calcular",
                    style: TextStyle(
                        fontSize: 22,
                      color: Colors.white
                    ),
                  ),
                  onPressed: _calcular,
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                child: Text(
                  _textoResultado,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        )
      ),
    );

  }
}
